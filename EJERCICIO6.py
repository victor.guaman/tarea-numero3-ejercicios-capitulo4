#Ejercicio 6: Reescribe el programa de cálculo del salario, con tarifa-ymedia
#para las horas extras, y crea una función llamada calculo_salario
#Que reciba dos parámetros (horas y tarifa).
#AUTOR:VICTOR GUAMAN

def calcular_salario(hora, tarifa):
    if  hora >= 40:

        horasextras= hora - 40

        tarifaextra = (0.5 * tarifa) + tarifa

        salario = (tarifaextra * horasextras) + (hora * tarifa)

    else:

        salario = tarifa * hora

    return salario


if __name__ == '__main__':

    while True:

        try:

            hora = int(input("ingresse las horas:  "))

            while True:

                try:

                    tarifa = float(input("ingrese la tarifa por hora trabajada : "))

                    salario = calcular_salario(hora, tarifa)

                    print('Su salario  a cobrar es: ', salario)

                    break

                except ValueError:

                    print("dato incorrecto ingrese un numero por favor:")

            break

        except ValueError:

            print("dato incorrecto solo se aceptan numero , ingrese un numero por favor:")
