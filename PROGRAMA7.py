#Ejercicio 7: Reescribe el programa de calificaciones del capítulo anterior
#usando una función llamada calcula_calificacion, que reciba una
#puntuación como parámetro y devuelva una calificación como cadena
#AUTOR:VICTOR GUAMAN.
#Puntuación Calificación
#> 0.9 Sobresaliente
#> 0.8 Notable
#> 0.7 Bien
#> 0.6 Suficiente
#<= 0.6 Insuficiente


def calcular_calificacion(cal):

    if 0 <= cal <= 1:

        if cal >= 0.9:

            print('Sobresaliente')

        elif cal >= 0.8:

            print('Notable')

        elif cal >= 0.7:

            print('Bien')

        elif cal >= 0.6:

            print('Suficiente')

        elif cal < 0.6:

            print(' Insuficiente')

    else:

        print('su puntuacion no es correcta:')

    return cal


if __name__ == '__main__':

    while True:

        try:

            califi = float(input('ingrese una calificacion:'))

            print(calcular_calificacion(califi))

            break

        except ValueError:

            print('dato erroneo por favor ingrese solo numeros:')

